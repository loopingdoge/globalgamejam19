﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager> {

    public float maxTime;
    public float countdownTimer;
    public int currentLevel = -1;

    public int currentGoalIndex = -1;
    public string currentGoalText; //TO DO
    public Level.GoalsList currentGoal;

    public List<Enemy> enemyPrefabsList;
    public List<Level> levels;
    public List<Vector3> waypoints;
    public Player player;
    public Transform playerSpawn;

    public delegate void VoidDelegate();
    public static event VoidDelegate initialized;

    public List<GameObject> floors;

    public int currentFloor = 0;

    public CanvasGroup pauseMenu;
    public CanvasGroup gameOverMenu;

    private bool isPaused = false;
    private bool isGameOver = false;
    private List<Enemy> enemyInstances = new List<Enemy>();

    void Start() {
        countdownTimer = maxTime;
        nextLevel();
        if (initialized != null) {
            initialized();
        }
        for (int i = 0; i < floors.Count; i++) {
            if (i != currentFloor) {
                floors[i].SetActive(false);
            }
        }
    }

    void Update() {
        if (countdownTimer > 0) {
            countdownTimer -= Time.deltaTime;
        } else {
            gameOver();
        }

        if (Input.GetButtonDown("Cancel")) {
            if (isPaused) {
                resume();
            } else {
                pause();
            }
        }

        if (isGameOver && Input.GetButtonDown("Restart")) {
            resetLevel();
        }
    }

    void resetLevel() {
        isGameOver = false;
        Time.timeScale = 1;
        StartCoroutine(hideMenu(gameOverMenu));
        loadLevel();
    }

    void nextLevel() {
        currentLevel++;
        loadLevel();
    }

    void loadLevel() {
        foreach (Enemy enemyInstance in enemyInstances) {
            Destroy(enemyInstance.gameObject);
        }
        enemyInstances.Clear();

        currentGoalIndex = 0;

        player.transform.position = playerSpawn.transform.position;
        player.transform.rotation = Quaternion.identity;

        Level l = levels[currentLevel];

        maxTime = l.maxTime;
        countdownTimer = maxTime;
        foreach (EnemyData enemy in l.enemies) {
            foreach (Enemy enemyPrefabObserved in enemyPrefabsList) {
                if (enemyPrefabObserved.name.Contains(enemy.nameEnemy)) {
                    Enemy enemyPrefab = enemyPrefabObserved;
                    Enemy enemyInstance = Instantiate(enemyPrefab, enemy.startPosition, Quaternion.identity);
                    enemyInstance.enemyData = enemy;
                    enemyInstances.Add(enemyInstance);
                    break;
                }
            }
        }
        setGoal();
    }

    void setGoal() {
        if (currentGoalIndex < levels[currentLevel].goals.Length) {
            currentGoal = levels[currentLevel].goals[currentGoalIndex];
            currentGoalText = levels[currentLevel].goals[currentGoalIndex].goalTextRequirement;
        } else {
            Debug.Log("Fine");
        }
    }

    public void changeFloor(int nextFloor) {
        floors[currentFloor].SetActive(false);
        floors[nextFloor].SetActive(true);
        currentFloor = nextFloor;
    }

    public void gameOver() {
        StartCoroutine(showMenu(gameOverMenu));
        Time.timeScale = 0;
        isGameOver = true;
    }

    public Level.GoalsList getCurrentGoal() {
        return currentGoal;
    }

    public void pause() {
        Time.timeScale = 0;
        isPaused = true;
        StartCoroutine(showMenu(pauseMenu));
    }

    public void resume() {
        Time.timeScale = 1;
        isPaused = false;
        StartCoroutine(hideMenu(pauseMenu));
    }

    public void goalCompleted() {
        currentGoalIndex++;
        setGoal();
    }

    IEnumerator showMenu(CanvasGroup menu) {
        while (menu.alpha < 1) {
            menu.alpha += Time.unscaledDeltaTime * 3.0f;
            yield return null;
        }
    }

    IEnumerator hideMenu(CanvasGroup menu) {
        while (menu.alpha > 0) {
            menu.alpha -= Time.unscaledDeltaTime * 3.0f;
            yield return null;
        }
    }

}
