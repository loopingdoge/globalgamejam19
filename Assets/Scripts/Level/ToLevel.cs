﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToLevel : MonoBehaviour {

    public Vector3 positionToMove;
    public UnityEvent action;

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            action.Invoke();
            other.transform.position = positionToMove;
        }
    }
}
