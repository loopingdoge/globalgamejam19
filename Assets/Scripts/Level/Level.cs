﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New", menuName = "CreateLevels", order = 1)]
public class Level : ScriptableObject {

    public float maxTime;
    public List<EnemyData> enemies;
    [System.Serializable]
    public class GoalsList {
        public string goalTextRequirement;
        public Vector3 positionTransform;
    }

    public GoalsList[] goals;
}
