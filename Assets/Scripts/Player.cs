﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public delegate void EnterInteractable();
    public event EnterInteractable Interact;
    public float moveSpeed;
    public GameObject body;
    private Rigidbody rb;

    private bool isHiding = false;
    private GameObject hidingActiveObject;
    private GameObject hidingObject;

    void Start() {
        rb = this.GetComponent<Rigidbody>();
    }

    void KeyCommands() {
        if (!isHiding) {
            if (Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.JoystickButton0) || Input.GetKeyDown(KeyCode.JoystickButton16)) {
                if (Interact != null) {
                    Interact();
                }
            }
            float verticalAxis = Input.GetAxis("Vertical");
            float horizontalAxis = Input.GetAxis("Horizontal");
            Vector3 vertical = new Vector3(0, 0, -horizontalAxis);
            Vector3 horizontal = new Vector3(verticalAxis, 0, 0);

            Vector3 direction = vertical + horizontal;
            if (verticalAxis != 0 || horizontalAxis != 0) {
                transform.forward = Vector3.Normalize(direction);
            }
            this.GetComponent<Rigidbody>().velocity = direction * moveSpeed;
        } else {
            rb.velocity = Vector3.zero;
            if (Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.JoystickButton0) || Input.GetKeyDown(KeyCode.JoystickButton16)) {
                ComeBack();
            }
        }
    }

    void Update() {
        KeyCommands();
    }

    public void Hide(GameObject hideActiveObject, GameObject hideObject) {
        hidingActiveObject = hideActiveObject;
        hidingObject = hideObject;
        isHiding = true;
        rb.useGravity = false;
        body.SetActive(false);
    }

    public void ComeBack() {
        hidingActiveObject.SetActive(false);
        hidingObject.SetActive(true);
        isHiding = false;
        rb.useGravity = true;
        body.SetActive(true);
    }
}
