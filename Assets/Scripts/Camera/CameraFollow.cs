﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    public float time = 0.3f;

    private Vector3 offset;
    private Vector3 velocity = Vector3.zero;

    void Start() {
        offset = transform.position - target.position;
    }

    private void LateUpdate() {
        Vector3 targetPosition = target.position + offset;
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, time);
    }
}
