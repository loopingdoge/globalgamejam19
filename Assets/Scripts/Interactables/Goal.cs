﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : Interactable {

    private bool hasInteracted = false;

    protected override void Interaction() {
        var distanceToCurrGoal = Vector3.Distance(LevelManager.Instance.player.transform.position, LevelManager.Instance.currentGoal.positionTransform);
        if (distanceToCurrGoal <= 2.0f) {
            if (!hasInteracted) {
                stopInteracting();
                hasInteracted = true;
                LevelManager.Instance.goalCompleted();
            }
        }
    }

}
