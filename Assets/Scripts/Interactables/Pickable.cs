﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class Pickable : MonoBehaviour {

    void OnTriggerEnter(Collider other) {
        if (other.tag.Contains("Player")) {
            var distanceToCurrGoal = Vector3.Distance(LevelManager.Instance.player.transform.position, LevelManager.Instance.currentGoal.positionTransform);
            if (distanceToCurrGoal <= 2.0f) {
                LevelManager.Instance.goalCompleted();
                Destroy(this.gameObject);
            }
        }
    }
}
