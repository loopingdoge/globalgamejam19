﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMesh : Interactable {

    public GameObject normalMesh;
    public GameObject hidingMesh;

    void Start() {
        mRenderer = normalMesh.gameObject.GetComponent<Renderer>();
        mMaterial = mRenderer.sharedMaterial;
    }

    protected override void Interaction() {
        LevelManager.Instance.player.Hide(hidingMesh, normalMesh);
        normalMesh.SetActive(false);
        hidingMesh.SetActive(true);
    }
}
