using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public abstract class Interactable : MonoBehaviour {

    public float interactionAngle = 40f;
    public Material outlineMaterial;

    protected Renderer mRenderer;
    protected Material mMaterial;

    private float triggerStayCooldown = 0.2f;
    private float elapsedTimeFromTriggerStay = 0.0f;

    void Start() {
        mRenderer = GetComponent<Renderer>();
        mMaterial = mRenderer.sharedMaterial;
    }

    void Update() {
        elapsedTimeFromTriggerStay += Time.deltaTime;
    }

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            LevelManager.Instance.player.Interact += onInteract;
            mRenderer.sharedMaterial = outlineMaterial;
        }
    }

    void OnTriggerStay(Collider other) {
        if (elapsedTimeFromTriggerStay >= triggerStayCooldown) {
            elapsedTimeFromTriggerStay = 0.0f;
            var angle = AngleBeetweenPlayerAndMe();
            if (angle < interactionAngle) {
                mRenderer.sharedMaterial = outlineMaterial;
            } else {
                mRenderer.sharedMaterial = mMaterial;
            }
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")) {
            LevelManager.Instance.player.Interact -= onInteract;
            mRenderer.sharedMaterial = mMaterial;
        }
    }

    void onInteract() {
        var angle = AngleBeetweenPlayerAndMe();
        if (angle < interactionAngle && Vector3.Distance(transform.position, LevelManager.Instance.player.transform.position) < 2.0f) {
            Interaction();
        }
    }

    protected void stopInteracting() {
        gameObject.SetActive(false);
        LevelManager.Instance.player.Interact -= onInteract;
        mRenderer.sharedMaterial = mMaterial;
    }

    private float AngleBeetweenPlayerAndMe() {
        var playerToObject = transform.position - LevelManager.Instance.player.transform.position;
        var angle = Vector3.Angle(playerToObject, LevelManager.Instance.player.transform.forward);
        return angle;
    }

    protected abstract void Interaction();
}
