﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSwitch : Interactable {

    public Light mLight;

    protected override void Interaction() {
        mLight.enabled = !mLight.enabled;
        Debug.Log("Change light");
    }

}
