﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Day : MonoBehaviour
{
    private TextMeshProUGUI dayText;

    void dayTextSet(string text)
    {
        if (text.CompareTo(dayText.text) != 0)
        {
            dayText.text = "Day " + text;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        dayText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        dayTextSet((LevelManager.Instance.currentLevel + 1).ToString());
    }
}
