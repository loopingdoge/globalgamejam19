﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    private TextMeshProUGUI timerText;
    private float timer;
    private int minutes;
    private int seconds;
    private string time;

    void Start()
    {
        timerText = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        
        timer = LevelManager.Instance.countdownTimer;
        minutes = Mathf.FloorToInt(timer / 60f);
        seconds = Mathf.FloorToInt(timer - minutes * 60);
        time = string.Format("{0:00} : {1:00}", minutes, seconds);
        if (timer <= 0)
        {
            time = string.Format("{0:00} : {1:00}", 0, 0);
        }
    
        timerText.text = time;

    }
}
