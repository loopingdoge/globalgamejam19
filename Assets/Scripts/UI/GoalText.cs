﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GoalText : MonoBehaviour {
    private TextMeshProUGUI goalText;

    void goalTextSet(string text) {
        if (text.CompareTo(goalText.text) != 0) {
            goalText.text = LevelManager.Instance.currentGoalText;
        }
    }

    void Start() {
        goalText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update() {
        goalTextSet(LevelManager.Instance.currentGoalText);
    }
}
