﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New", menuName = "CreateEnemyData", order = 0)]
public class EnemyData : ScriptableObject {
    public string nameEnemy;
    public Vector3 startPosition;
    public List<Vector3> waypoints;
}
