﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    // enum EnemyState {
    //     Walking,
    //     Waiting,
    //     Alerted
    // }

    public GameObject alertedIcon;
    public GameObject playerFoundIcon;
    public EnemyData enemyData;
    public float waitTime = 4f;
    public Player player;
    public float viewDistance = 5f;
    public float viewAngle = 80f;

    NavMeshAgent agent;
    Vector3 currentDestination;
    List<Vector3> waypoints;
    int destinationIndex = 0;
    float deltaDistance = 2.0f;
    bool started = false;

    private float timeWaited = 0.0f;
    private bool isWaiting = false;
    private bool isAlerted = false;

    void Start() {
        startMoving();
        player = LevelManager.Instance.player;
    }

    void Update() {
        if (started) {
            // If the player is seen, game over
            if (isPlayerOnSight(viewAngle, 4f)) {
                seePlayer();
            }

            // If the player is seen in the narrow cone, the enemy is alerted
            // if (!isAlerted) {
            if (isPlayerOnSight(viewAngle, 7.5f)) {
                startAlert(player.transform.position);
            }
            // }

            // If the enemy has reached his destination, he goes to the next waypoint
            if (Vector3.Distance(transform.position, currentDestination) < deltaDistance) {
                destinationIndex++;
                if (waypoints.Count - 1 < destinationIndex) {
                    destinationIndex = 0;
                }
                currentDestination = waypoints[destinationIndex];
                startWaiting();
            }

            // If the enemy is waiting and enough time has passed, he stops waiting
            if (isWaiting) {
                timeWaited += Time.deltaTime;
                if (timeWaited >= waitTime) {
                    stopWaiting();
                }
            }

            // If the enemy is alerted, and he has reached his destination, he stops alerting
            if (isAlerted) {
                if (Vector3.Distance(transform.position, agent.destination) <= 1.0f) {
                    stopAlert();
                }
            }
        }
    }

    void startMoving() {
        agent = GetComponent<NavMeshAgent>();
        waypoints = enemyData.waypoints;
        currentDestination = waypoints[destinationIndex];
        agent.destination = currentDestination;
        started = true;
    }

    void seePlayer() {
        started = false;
        alertedIcon.SetActive(false);
        playerFoundIcon.SetActive(true);
        agent.destination = transform.position;
        LevelManager.Instance.gameOver();
    }

    void startWaiting() {
        isWaiting = true;
        timeWaited = 0.0f;
    }

    void stopWaiting() {
        timeWaited = 0.0f;
        isWaiting = false;
        alertedIcon.SetActive(false);
        transform.LookAt(currentDestination);
        agent.destination = currentDestination;
    }

    void startAlert(Vector3 position) {
        isAlerted = true;
        alertedIcon.SetActive(true);
        agent.destination = position;
    }

    void stopAlert() {
        isAlerted = false;
        startWaiting();
    }

    bool isPlayerOnSight(float sightAngle, float distance) {
        var res = false;
        var dist = Vector3.Distance(player.transform.position, transform.position);
        var playerToMe = (player.transform.position - transform.position).normalized;
        var angle = Vector2.Angle(new Vector2(transform.forward.normalized.x, transform.forward.normalized.z), new Vector2(playerToMe.x, playerToMe.z));
        Debug.DrawRay(transform.position, (Quaternion.Euler(0.0f, sightAngle, 0.0f) * transform.forward) * distance);
        Debug.DrawRay(transform.position, (Quaternion.Euler(0.0f, -sightAngle, 0.0f) * transform.forward) * distance);
        if (dist <= distance && angle <= sightAngle) {
            RaycastHit hit;
            if (Physics.Raycast(
                transform.position,
                player.transform.position - transform.position,
                out hit,
                distance
            )) {
                if (hit.collider.CompareTag("Player")) {
                    res = true;
                }
            }
        }
        return res;
    }

}
